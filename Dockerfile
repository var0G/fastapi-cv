FROM python:3.10.5

WORKDIR /app

COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade  -r /code/requirements.txt

RUN apt-get update && apt-get install -y libgl1

COPY /app .

CMD ["python3", "main.py"]
