# Api to computer vision project
This project aims to build a api to act as a interface to users, where they can
upload a image and get results of a pre train model that predict fruits class and
returns the nutritional table.

- You need to rewrite the path of the `.pt` models that you desire to use

## Requirements
- python==3.10.5
- (recommend) `virtualenv env`
- ` pip install -r requirements.txt `

## Start the api
- `python3 main.py`
- Navigate to  http://0.0.0.0:8000/docs
## Optional
- You can replace the app/best.pt trained model for your own yolov5 model

##  Docker demo
- In the folder where the Dockerfile is located run: `docker build -t <tag-name> .`
- Run the image `docker run -d --name <name> -p 8080:8000 <image>`
- clone this other repository: https://gitlab.com/v1nte/fruits-svelte 
- Inside the repo build the image with docker `docker buid -t <tag-name>`
- Run the container `docker run -d --name container -p 3000:80 image`
- Navigate to http://localhost:3000 and test an image.
