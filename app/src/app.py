from fastapi import FastAPI, File, UploadFile, HTTPException
from src.utils import static_data, process_image
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
        CORSMiddleware,
        allow_origins=["http://localhost:8080", "http://localhost:3000"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
)


@app.post("/uploadimage")
async def upload_file(file: UploadFile):
    """Endpoint to upload files and return the results
    based on the prediction

    Parameters
    ----------
    file : UploadFile
        image file on bytes format
    Returns
    -------
    JSON
        results of nutritional information based on the object detection
        confidence.
    """
    if not (file.content_type.startswith("image/")):
        raise HTTPException(status_code=415, detail="Format not allowed")
    send_file = await file.read()
    results = process_image(send_file)
    nutritional_info = static_data(results)
    return {"Information": nutritional_info}
