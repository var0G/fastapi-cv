import io
import cv2
import json
import yolov5
import numpy as np
import PIL.Image as Image

model = yolov5.load("best.pt")


def static_data(results: list) -> tuple:
    """Process the results of the object model to compare results and base on
    the confidence return the corresponding values.

    Parameters
    ----------
    results : list
        list of results based on the model detection in JSON format
    Returns
    -------
    dict
        results of nutritional information based on the object detection confidence
    """

    with open("nutritional.json", "r") as file:
        json_file = json.load(file)
    try:
        a = results[0]["confidence"]
        res = results[0]
    except IndexError:
        res = {"Error": "No detection was found"}
        return res
    for i in results:
        if i["confidence"] >= a:
            a = i["confidence"]
            res = i
    both_results = (json_file[res["name"]], results)
    file.close()
    return both_results


def process_image(file_image: bytes) -> list:
    """Process the image on bytes to a format that the model can understand and
    return the results of the detection.

    Parameters
    ----------
    file_image : bytes
        image file in bytes to be process
    Returns
    -------
    list
        result or results of the detection model
    """
    img = np.array(Image.open(io.BytesIO(file_image)))
    resize_img = cv2.resize(img, (640, 640), interpolation=cv2.INTER_LINEAR)
    results = model(resize_img)
    detect_results = results.pandas().xyxy[0].to_json(orient="records")
    detect_results = json.loads(detect_results)
    return detect_results
